#!/bin/sh

docker build -t layers:0.1 .
docker run layers:0.1
docker history layers:0.1 > history.txt
docker save -o layer.tar layers:0.1
